package test.task.repository;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

public class DigitCodeRepository {

    private Map<Character, String> repository;

    public DigitCodeRepository() {
        repository = new HashMap<>();

        repository.put('a', "2");
        repository.put('b', "22");
        repository.put('c', "222");
        repository.put('d', "3");
        repository.put('e', "33");
        repository.put('f', "333");
        repository.put('g', "4");
        repository.put('h', "44");
        repository.put('i', "444");
        repository.put('j', "5");
        repository.put('k', "55");
        repository.put('l', "555");
        repository.put('m', "6");
        repository.put('n', "66");
        repository.put('o', "666");
        repository.put('p', "7");
        repository.put('q', "77");
        repository.put('r', "777");
        repository.put('s', "7777");
        repository.put('t', "8");
        repository.put('u', "88");
        repository.put('v', "888");
        repository.put('w', "9");
        repository.put('x', "99");
        repository.put('y', "999");
        repository.put('z', "9999");
        repository.put(' ', "0");
    }

    public Optional<String> getCode(char character) {
        return Optional.ofNullable(repository.get(character));
    }
}
