package test.task;

import com.google.inject.Guice;
import com.google.inject.Injector;
import test.task.config.AppModule;
import test.task.service.SentencesPrinter;
import test.task.service.SentenceProcessor;
import test.task.service.SentencesReader;

import java.util.List;

public class Application {

    public static void main(String[] args) {
        Injector injector = Guice.createInjector(new AppModule());
        SentenceProcessor sentenceProcessor = injector.getInstance(SentenceProcessor.class);
        SentencesReader sentencesReader = injector.getInstance(SentencesReader.class);
        SentencesPrinter sentencesPrinter = injector.getInstance(SentencesPrinter.class);
        List<String> processedCases = sentenceProcessor.process(sentencesReader.readAllSentences());
        sentencesPrinter.printResult(processedCases);
//        System.out.println(String.join("/n", processedCases));
    }
}
