package test.task.service;

import javax.inject.Inject;
import java.util.stream.IntStream;

public class SentenceTranscriber {

    private DigitCodeService digitCodeService;

    @Inject
    public SentenceTranscriber(DigitCodeService digitCodeService) {
        this.digitCodeService = digitCodeService;
    }

    public String transcribeToDigitSequence(String sentence) {
        StringBuilder result = new StringBuilder();
        IntStream.range(0, sentence.length())
                .mapToObj(charIndex -> digitCodeService.getCode(sentence.charAt(charIndex)))
                .forEach(digitCode -> result.append( endsWithSameDigit(result, digitCode) ? " " : "").append(digitCode));
        return result.toString();
    }

    private boolean endsWithSameDigit(StringBuilder result, String digitCode) {
        return result.toString().endsWith(digitCode.substring(0, 1));
    }
}
