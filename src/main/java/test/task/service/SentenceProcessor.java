package test.task.service;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class SentenceProcessor {

    private SentenceTranscriber sentenceTranscriber;

    @Inject
    public SentenceProcessor(SentenceTranscriber sentenceTranscriber) {
        this.sentenceTranscriber = sentenceTranscriber;
    }

    public List<String> process(List<String> sentences) {
        List<String> transcribedCasesCollector = new ArrayList<>();
        for (int caseCount = 0; caseCount < sentences.size(); caseCount++) {
            String result = sentenceTranscriber.transcribeToDigitSequence(sentences.get(caseCount));
            transcribedCasesCollector.add(format("Case #%d: %s", caseCount + 1, result));
        }
        return transcribedCasesCollector;
    }
}
