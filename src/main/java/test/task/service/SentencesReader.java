package test.task.service;

import lombok.SneakyThrows;

import javax.inject.Inject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class SentencesReader {

    private InputStream inputStream;

    @Inject
    public SentencesReader(InputStream inputStream) {
        this.inputStream = inputStream;
    }

    @SneakyThrows(IOException.class)
    public List<String> readAllSentences() {
        List<String> sentences = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
            int linesNumber = Integer.parseInt(reader.readLine());
            for (int linesCount = 0; linesCount < linesNumber; linesCount++) {
                sentences.add(reader.readLine());
            }
        }
        return sentences;
    }
}
