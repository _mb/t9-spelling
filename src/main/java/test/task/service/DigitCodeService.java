package test.task.service;

import test.task.repository.DigitCodeRepository;

import javax.inject.Inject;
import java.util.NoSuchElementException;

import static java.lang.String.format;

public class DigitCodeService {

    private DigitCodeRepository repository;

    @Inject
    public DigitCodeService(DigitCodeRepository repository) {
        this.repository = repository;
    }

    public String getCode(char character) {
        return repository.getCode(character).orElseThrow(
                () -> new NoSuchElementException(format("No mapping for char %s found", character)));
    }
}
