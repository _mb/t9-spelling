package test.task.service;

import javax.inject.Inject;
import java.io.*;
import java.util.List;

public class SentencesPrinter {

    private PrintStream printStream;

    @Inject
    public SentencesPrinter(PrintStream printStream) {
        this.printStream = printStream;
    }

    public void printResult(List<String> sentences) {
        if (sentences.isEmpty()) {
            return;
        }
        printStream.println(String.join("\n", sentences));
    }

}
