package test.task.config;

import com.google.inject.AbstractModule;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintStream;

public class AppModule extends AbstractModule {
    @Override
    protected void configure() {

        bind(InputStream.class).toInstance(System.in);
        bind(PrintStream.class).toInstance(System.out);
    }
}
