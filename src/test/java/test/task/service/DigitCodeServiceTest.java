package test.task.service;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.function.Executable;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;
import test.task.repository.DigitCodeRepository;

import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.never;

@ExtendWith(MockitoExtension.class)
class DigitCodeServiceTest {

    @Mock
    private DigitCodeRepository repository;

    @InjectMocks
    private DigitCodeService digitCodeService;

    @Test
    public void testReturnsDigitCode() {
        //given
        char character = 'a';
        String expected = "111";

        Mockito.when(repository.getCode(anyChar())).thenReturn(Optional.of("111"));

        //when
        String actual = digitCodeService.getCode(character);

        //then
        Assertions.assertEquals(actual, expected);
    }

    @Test
    public void testThrowsNoSuchElementException() {
        //given
        char character = 'a';

        Mockito.when(repository.getCode(anyChar())).thenReturn(Optional.ofNullable(null));

        //when
        //then
        Assertions.assertThrows(NoSuchElementException.class, () -> digitCodeService.getCode(character));
    }

}