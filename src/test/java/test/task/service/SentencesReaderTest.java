package test.task.service;

import lombok.SneakyThrows;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.mockito.junit.jupiter.MockitoSettings;
import org.mockito.quality.Strictness;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.stream.Stream;

import static java.lang.String.format;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
@MockitoSettings(strictness = Strictness.LENIENT)
class SentencesReaderTest {

    @Test
    public void testReadZeroSentences() {
        //given
        SentencesReader sentencesReader = new SentencesReader(new ByteArrayInputStream("0\n".getBytes()));
        List<String> expected = List.of();

        //when
        List<String> actual = sentencesReader.readAllSentences();

        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testReadOneSentence() {
        //given
        SentencesReader sentencesReader = new SentencesReader(new ByteArrayInputStream(format("1\n%s", "hello worlds").getBytes()));
        List<String> expected = List.of("hello worlds");

        //when
        List<String> actual = sentencesReader.readAllSentences();

        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testReadOneLetter() {
        //given
        SentencesReader sentencesReader = new SentencesReader(new ByteArrayInputStream(format("1\n%s", "a").getBytes()));
        List<String> expected = List.of("a");

        //when
        List<String> actual = sentencesReader.readAllSentences();

        //then
        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testReadMultiplySentences() {
        //given
        String[] sentences = {"hi", "nice to meet", "terrible weather"};
        @SuppressWarnings("RedundantCast")
        SentencesReader sentencesReader = new SentencesReader(
                new ByteArrayInputStream(format("3\n%s\n%s\n%s\n", (Object[]) sentences).getBytes()));
        List<String> expected = List.of(sentences);

        //when
        List<String> actual = sentencesReader.readAllSentences();

        //then
        Assertions.assertEquals(expected, actual);
    }

}