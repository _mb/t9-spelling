package test.task.service;

import lombok.SneakyThrows;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.util.List;

@ExtendWith(MockitoExtension.class)
class SentencesPrinterTest {

    private  ByteArrayOutputStream outputStream;

    @BeforeEach
    public void setUp() {
        outputStream = new ByteArrayOutputStream();
    }

    @SneakyThrows(IOException.class)
    @AfterEach
    public void close() {
        outputStream.close();
    }

    @Test
    public void testPrintsNothing() {
        //given
        SentencesPrinter sentencesPrinter = new SentencesPrinter(new PrintStream(outputStream));
        String expected = "";

        //when
        sentencesPrinter.printResult(List.of());

        //then
        Assertions.assertEquals(expected, outputStream.toString());
    }

    @Test
    public void testPrintsOneSentence() {
        //given
        SentencesPrinter sentencesPrinter = new SentencesPrinter(new PrintStream(outputStream));
        String expected = "a\n";

        //when
        sentencesPrinter.printResult(List.of("a"));

        //then
        Assertions.assertEquals(expected, outputStream.toString());
    }

    @Test
    public void testPrintsMultipleSentences() {
        //given
        SentencesPrinter sentencesPrinter = new SentencesPrinter(new PrintStream(outputStream));
        List<String> sentences = List.of("hi", "greet friends", "bye and cry");
        String expected = String.format("%s\n%s\n%s\n","hi", "greet friends", "bye and cry");

        //when
        sentencesPrinter.printResult(sentences);

        //then
        Assertions.assertEquals(expected, outputStream.toString());
    }

}