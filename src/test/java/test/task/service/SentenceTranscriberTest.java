package test.task.service;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyChar;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.times;

@ExtendWith(MockitoExtension.class)
class SentenceTranscriberTest {

    @Mock
    private DigitCodeService digitCodeService;

    @InjectMocks
    private SentenceTranscriber sentenceTranscriber;

    @Test
    public void testTranscribeOneLetterSentence() {
        //given
        String sentence = "!";
        String expectedDigitCodes = "9";

        Mockito.when(digitCodeService.getCode(anyChar())).thenReturn("9");

        //when
        String actual = sentenceTranscriber.transcribeToDigitSequence(sentence);

        //then
        assertEquals(expectedDigitCodes, actual);
        Mockito.verify(digitCodeService, times(1)).getCode(anyChar());
    }

    @Test
    public void testTranscribeMultipleLettersSentence() {
        //given
        String sentence = "ave";
        String expectedDigitCodes = "123";

        Mockito.when(digitCodeService.getCode(anyChar())).thenReturn("1", "2", "3");

        //when
        String actual = sentenceTranscriber.transcribeToDigitSequence(sentence);

        //then
        assertEquals(expectedDigitCodes, actual);
        Mockito.verify(digitCodeService, times(3)).getCode(anyChar());
    }

    @Test
    public void testTranscribeSentenceWithRepetitiveLetters() {
        //given
        String sentence = "hello";
        String expectedDigitCodes = "123 34";

        Mockito.when(digitCodeService.getCode(anyChar())).thenReturn("1", "2", "3", "3","4");

        //when
        String actual = sentenceTranscriber.transcribeToDigitSequence(sentence);

        //then
        assertEquals(expectedDigitCodes, actual);
        Mockito.verify(digitCodeService, times(5)).getCode(anyChar());
    }

    @Test
    public void testTranscribeSentenceWithSameCodedLetters() {
        //given
        String sentence = "super";
        String expectedDigitCodes = "1 11145555 5";

        Mockito.when(digitCodeService.getCode(anyChar())).thenReturn("1", "111", "4", "5555","5");

        //when
        String actual = sentenceTranscriber.transcribeToDigitSequence(sentence);

        //then
        assertEquals(expectedDigitCodes, actual);
        Mockito.verify(digitCodeService, times(5)).getCode(anyChar());
    }

    @Test
    public void testTranscribeWithNoTrailingSpaces() {
        //given
        String sentence = "no";
        String expectedDigitCodes = "1 11";

        Mockito.when(digitCodeService.getCode(anyChar())).thenReturn("1", "11");

        //when
        String actual = sentenceTranscriber.transcribeToDigitSequence(sentence);

        //then
        assertEquals(expectedDigitCodes, actual);
        Mockito.verify(digitCodeService, times(2)).getCode(anyChar());
    }
}