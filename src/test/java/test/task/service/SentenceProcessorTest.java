package test.task.service;


import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.List;

import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class SentenceProcessorTest {

    @Mock
    private SentenceTranscriber sentenceTranscriber;

    @InjectMocks
    private SentenceProcessor sentenceProcessor;

    @Test
    public void testProcessNothing() {
        //given
        List<String> sentences = List.of();
        List<String> expected = List.of();

        //when
        List<String> cases = sentenceProcessor.process(sentences);

        //then
        Assertions.assertEquals(expected, cases);
        Mockito.verify(sentenceTranscriber, never()).transcribeToDigitSequence(anyString());
    }

    @Test
    public void testProcessOneCase() {
        //given
        List<String> sentences = List.of("hi");
        List<String> expected = List.of("Case #1: 011");

        Mockito.when(sentenceTranscriber.transcribeToDigitSequence(anyString())).thenReturn("011");
        //when
        List<String> cases = sentenceProcessor.process(sentences);

        //then
        Assertions.assertEquals(expected, cases);
        Mockito.verify(sentenceTranscriber, times(1)).transcribeToDigitSequence(anyString());
    }

    @Test
    public void testProcessMultipleCases() {
        //given
        List<String> sentences = List.of("hi", "sweet", "demo");
        List<String> expected = List.of("Case #1: 011", "Case #2: 22111", "Case #3: 33444 4");

        Mockito.when(sentenceTranscriber.transcribeToDigitSequence(anyString())).thenReturn("011", "22111", "33444 4");
        //when
        List<String> cases = sentenceProcessor.process(sentences);

        //then
        Assertions.assertEquals(expected, cases);
        Mockito.verify(sentenceTranscriber, times(3)).transcribeToDigitSequence(anyString());
    }
}